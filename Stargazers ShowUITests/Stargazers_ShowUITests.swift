//
//  Stargazers_ShowUITests.swift
//  Stargazers ShowUITests
//
//  Created by Romeo Violini on 25/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import XCTest

class Stargazers_ShowUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    //Test simply if the controller for typing owner and repo to search is called and dismissed touching outside
    func testPopoverCall() {
        let app = XCUIApplication()
        let searchButton = app.navigationBars["Stargazers"].buttons["Search"]
        searchButton.tap()
        app/*@START_MENU_TOKEN@*/.otherElements["PopoverDismissRegion"]/*[[".otherElements[\"dismiss popup\"]",".otherElements[\"PopoverDismissRegion\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        searchButton.tap()
        app.buttons["Cancel"].tap()
    }
    //Test a simple usage for a search typing an owner and repo and check the behaviour of navigation bar title
    func testSimpleUsageTestAndCheckNavigationBarTitleBehaviour() {
        let app = XCUIApplication()
        app.navigationBars["Stargazers"].buttons["Search"].tap()
        
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1)
        let element = element2.children(matching: .other).element(boundBy: 2).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element
        element.children(matching: .textField).element(boundBy: 0).typeText("rs")
        
        let textField = element.children(matching: .textField).element(boundBy: 1)
        textField.tap()
        
        let shiftButton = app/*@START_MENU_TOKEN@*/.buttons["shift"]/*[[".keyboards",".buttons[\"maiuscole\"]",".buttons[\"shift\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
        shiftButton.tap()
        textField.typeText("S")
        shiftButton.tap()
        textField.typeText("D")
        shiftButton.tap()
        textField.typeText("Web")
        shiftButton.tap()
        textField.typeText("Image")
        element2.buttons["Search"].tap()
        //Check the behaviour of naviagtion bar title
        //Its title must be changed from Stargazers title
        XCTAssertFalse(app.navigationBars["Stargazers"].exists)
        //To
        XCTAssertTrue(app.navigationBars["rs/SDWebImage"].exists)
    }
    
}
