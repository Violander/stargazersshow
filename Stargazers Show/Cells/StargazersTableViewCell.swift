//
//  StargazersTableViewCell.swift
//  Stargazers Show
//
//  Created by Romeo Violini on 25/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class StargazersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
}
