//
//  StargazersTableViewController.swift
//  Stargazers Show
//
//  Created by Romeo Violini on 25/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

class StargazersTableViewController: UITableViewController, UIPopoverPresentationControllerDelegate, SearchRepoPopupDelegate {
    //Stargazers array to update table view
    var stargazers: [Stargazer]!
    //A loading View to show during Updates
    var loadingView: UIView!
    //A cache to manage downloaded profile pictures
    let cache = NSCache<NSString, UIImage>()
    //This is the link to retrieve more stargazers data if exists
    var nextLink: String?
    //This is the current link to retrieve stargazers data
    var currentLink: String?
    //This is a lock to prevent race condition calls during scroll down
    var loadMoreContentLock = false
    //This is a message for main info when data is not retrieved
    var infoMessage: String!
    //This is default image for profile picture
    let defaultImage = UIImage(named: "defaultProfileImage")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Initialize info Message
        self.infoMessage =  "Tap Search Icon to Search stargazers for a Repo"
        //Initialize Cache Limit
        cache.countLimit = 90
        //Initialize Users
        self.stargazers = []
        //Initialize Loading View
        self.loadingView = UIView()
        self.loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        self.view.addSubview(self.loadingView)
        self.loadingView.translatesAutoresizingMaskIntoConstraints = false
        //Set Loading View Contraints
        NSLayoutConstraint(item: self.loadingView, attribute: .top, relatedBy: .equal,
                           toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self.loadingView, attribute: .leading, relatedBy: .equal,
                           toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self.loadingView, attribute: .width, relatedBy: .equal,
                           toItem: self.view, attribute: .width, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self.loadingView, attribute: .height, relatedBy: .equal,
                           toItem: self.view, attribute: .height, multiplier: 1.0, constant: 0).isActive = true
        //Create a loading Indicator
        let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        loadingIndicator.color = UIColor.darkGray
        loadingIndicator.startAnimating()
        self.loadingView.addSubview(loadingIndicator)
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        //Set Indicator Constraints
        NSLayoutConstraint(item: loadingIndicator, attribute: .centerX, relatedBy: .equal,
                           toItem: self.loadingView, attribute: .centerX,
                           multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: loadingIndicator, attribute: .centerY, relatedBy: .equal,
                           toItem: self.loadingView, attribute: .centerY,
                           multiplier: 1.0, constant: 0.0).isActive = true
        //Hide Loading View till gets call
        self.loadingView.isHidden = true
        
        //Configure TableVIew
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshStargazers(_:)), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        self.tableView.allowsSelection = false
    }
    
    @objc private func refreshStargazers(_ sender: Any) {
        // Fetch Stargazers Data
        self.loadStargazers(link: self.currentLink ?? "", reload: true)
    }
    
    func loadStargazers(link: String, reload: Bool) {
        //Start searching stargazers
        self.loadingView.isHidden = false
        //Start Searching stargazers
        self.fetchAllStargazers(link: link, completion: { (retrievedStargazers, error) in
            if retrievedStargazers != nil {
                //If must reload data
                if (reload) {
                    //Set stargazers with retrievd data
                    self.stargazers = retrievedStargazers
                } else {
                    //Extend stargazers with retrieved data
                    self.stargazers.append(contentsOf: retrievedStargazers!)
                }
                if self.stargazers.count == 0 {
                    self.infoMessage = "No Stargazers Found for this Owner's Repo"
                }
                
            } else {
                //Update View
                //Show Error
                self.stargazers = []
                if error != nil {
                    self.infoMessage = error!
                } else {
                    self.infoMessage = "Generic Error"
                }
            }
            //Update UI
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                self.tableView.reloadData()
                self.tableView.refreshControl!.endRefreshing()
            }
        })
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.stargazers.count > 0 {
            return self.stargazers.count
        } else {
            //I always want show a cell with a message instead of empty tableview
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //If there are any stargazers
        if self.stargazers.count > 0 {
            //Get the current stargazer
            let user = self.stargazers[indexPath.row]
            //Get the current cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "StargazersCell", for: indexPath) as! StargazersTableViewCell
            //configure cell name
            cell.labelUsername.text = user.username
            //Check if stargazer's image is nil
            if user.image == nil {
                //Set the dowloaded state of stargezer to new, so his piture must be downloaded
                user.state = .new
            }
            
            switch user.state {
            case .new:
                //Set the default profile image
                cell.imageViewProfile.image = self.defaultImage
                //Show a loading indicator for profile picture
                cell.loadingIndicator.startAnimating()
                //Start downloading profile picture
                self.downloadProfilePicture(url: URL(string: user.profileImageLink)!, indexPath: indexPath)
                break
            case .downloaded, .failed:
                //Stop showing loading indicator
                cell.loadingIndicator.stopAnimating()
                //Set the user image to cell
                cell.imageViewProfile.image = user.image
                break
            }
            return cell
        } else {
            //Show the cell with the current info message
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as! InfoTableVIewCell
            cell.labelInfo.text = self.infoMessage
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //Set the height of a tableview cell to custom size
        return 66.0
    }
    
    // MARK: - ScrollViewDelegate
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Check the maximum content offset
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        //Check if the offset between maximum offset and current content offset is less than 10
        if maximumOffset - scrollView.contentOffset.y <= 10.0 {
            //If there isn't a lock and the nextLink exists
            if !self.loadMoreContentLock && self.nextLink != nil {
                //Load More Data
                self.loadStargazers(link: self.nextLink!, reload: false)
            }
        }
    }
    
    // MARK: - Tasks Operations
    // Get All Stargazers From a given link
    public func fetchAllStargazers(link: String, completion: @escaping ([Stargazer]?, String?) -> Void) {
        self.loadMoreContentLock = true
        //Check if the url is a valid url
        guard let url = URL(string: link) else {
            self.loadMoreContentLock = false
            completion(nil, "Invalid Url")
            return
        }
        //Create and Manage settings of Url Request to retrieve correct data
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        //Start a task from current session to download data checking error
        URLSession.shared.dataTask(with: urlRequest)
        { (data, response, error) -> Void in
            //Check If response is valid
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200 else {
                    self.loadMoreContentLock = false
                    completion(nil, "Repo not found: Bad Request")
                    return
            }
            //Check if response returns errors
            guard error == nil else {
                self.loadMoreContentLock = false
                completion(nil,error!.localizedDescription)
                return
            }
            //Tra to serialize data
            guard
                let data = data,
                let json = try? JSONSerialization.jsonObject(with: data) as? [[String: Any]]
                else {
                    self.loadMoreContentLock = false
                    completion(nil,"Repo not found: Bad Data Received")
                    return
            }
            //Check and Parse the next link from the header of response
            self.nextLink = nil
            if let link = httpURLResponse.allHeaderFields["Link"] as? String {
                let parsed = Parser.parseGithubLinkHeader(input: link)
                if let next = parsed["next"] {
                    self.nextLink = next
                }
            }
            //Map data to a stargazers array and return data
            let retrievedStargazers = json!.flatMap({ (userDict) -> Stargazer? in
                return Stargazer(jsonData: userDict)
            })
            self.loadMoreContentLock = false
            completion(retrievedStargazers, nil)
            }.resume()
    }
    
    //Check if image is already in cache otherwise try to download from the given url and update the user state in the corresponding row
    func downloadProfilePicture(url: URL, indexPath: IndexPath) {
        //Check if image is already in cache setting it to the corresponding stargazer and setting his downloadState to .downloaded
        if let image = self.cache.object(forKey: url.path as NSString) {
            self.stargazers[indexPath.row].state = .downloaded
            self.stargazers[indexPath.row].image = image
            //Update UI
            DispatchQueue.main.async() {
                self.tableView.reloadRows(at: [indexPath], with: .fade)
                return
            }
        }
        //Start Retrieving Image Data
        URLSession.shared.dataTask(with: url) { data, response, error in
            //Check If response is valid
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                    //Set the Download State of a Stargazer to Failed
                    self.stargazers[indexPath.row].state = .failed
                    //Update UI
                    DispatchQueue.main.async() {
                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                    }
                    return
            }
            //Set image to corresponding stargazer,
            self.stargazers[indexPath.row].state = .downloaded
            self.stargazers[indexPath.row].image = image
            self.cache.setObject(image, forKey: url.path as NSString)
            //Update UI
            DispatchQueue.main.async() {
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
            }.resume()
    }
    
    // MARK: - SearchRepoPopupDelegate
    //Set Data with data retrievd from the popover
    func onSearchTapped(owner: String, repo: String) {
        self.currentLink = "https://api.github.com/repos/\(owner)/\(repo)/stargazers"
        self.nextLink = nil
        self.navigationItem.title = "\(owner)/\(repo)"
        self.tableView.reloadData()
        self.loadStargazers(link: self.currentLink!, reload: true)
    }
    
    // MARK: - Popover Preentation Delegates
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    
    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Set Current controller as delegate of popoverPresentationController of the destination controller
        segue.destination.popoverPresentationController!.delegate = self
        //Set Current controller as delegate of the destination controller
        (segue.destination as! SearchRepoViewController).delegate = self
    }
}
