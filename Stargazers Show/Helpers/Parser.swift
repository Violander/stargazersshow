//
//  Parser.swift
//  Stargazers Show
//
//  Created by Romeo Violini on 25/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import Foundation

class Parser {
    //This is a helper method to parse the header link retrieved in the github response
    static func parseGithubLinkHeader(input: String) -> [String: String] {
        guard (input.count != 0) else {
            return [:]
        }
        // Split parts by comma
        let parts = input.components(separatedBy: ",")
        var links = [String: String]()
        // Parsing data ro assign rel to link
        for p in parts {
            var section = p.components(separatedBy: ";")
            guard section.count == 2 else {
                return [:]
            }
            let urlRegex = try! NSRegularExpression(pattern: "<(.*)>", options: [])
            let url = urlRegex.stringByReplacingMatches(in: section[0], options: [], range: NSRange(location: 0, length: section[0].count), withTemplate: "$1").trimmingCharacters(in: CharacterSet.init(charactersIn: " "))
            
            let nameRegex = try! NSRegularExpression(pattern: "rel=\"(.*)\"", options: [])
            let name = nameRegex.stringByReplacingMatches(in: section[1], options: [], range: NSRange(location: 0, length: section[1].count), withTemplate: "$1").trimmingCharacters(in: CharacterSet.init(charactersIn: " "))
            
            links[name] = url;
        }
        return links;
    }
}
