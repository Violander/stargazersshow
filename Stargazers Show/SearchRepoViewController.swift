//
//  SearchRepoViewController.swift
//  Stargazers Show
//
//  Created by Romeo Violini on 25/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

protocol SearchRepoPopupDelegate: class {
    func onSearchTapped(owner: String, repo: String)
}

class SearchRepoViewController: UIViewController, UITextFieldDelegate {
    
    let TagOwner = 0
    let TagRepo = 1
    
    var owner: String!
    var repo: String!
    weak var delegate: SearchRepoPopupDelegate?
    
    @IBOutlet weak var textFieldOwner: UITextField!
    @IBOutlet weak var textFieldRepo: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldOwner.becomeFirstResponder()
    }
    
    // MARK: - UIButton Methods
    @IBAction func onCancelTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSearchTap() {
        self.view.endEditing(true)
        //Check if items are valid
        if self.validateItem() {
            //Send items to delegate with items
            self.delegate?.onSearchTapped(owner: self.owner, repo: self.repo)
            self.dismiss(animated: true, completion: nil)
        } else {
            //Show a warning message alert
            let alert = UIAlertController(title: "Warning", message: "Fields cannot be empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    // MARK: - Textfield Delegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == TagOwner {
            self.owner = textField.text
        } else {
            self.repo = textField.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == TagOwner {
            self.textFieldRepo.becomeFirstResponder()
        } else {
            self.onSearchTap()
        }
        return true
    }
    
    // MARK: - Validation
    //Fileds mustn't be empty
    func validateItem() -> Bool {
        if self.owner == nil || self.owner == "" || self.repo == nil || self.repo == "" {
            return false
        }
        return true
    }
}
