//
//  Stargazer.swift
//  Stargazers Show
//
//  Created by Romeo Violini on 25/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import Foundation
import UIKit

class Stargazer {
    
    enum DownloadState {
        case new, downloaded, failed
    }
    
    var username: String!
    var profileImageLink: String!
    var state: DownloadState = .new
    //Weak Image Managed By Cache
    weak var image: UIImage?
    
    
    init(jsonData: [String:Any]) {
        self.username = jsonData["login"] as! String
        self.profileImageLink = jsonData["avatar_url"] as! String
    }
}
