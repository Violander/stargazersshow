//
//  Stargazers_ShowTests.swift
//  Stargazers ShowTests
//
//  Created by Romeo Violini on 25/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import XCTest
@testable import Stargazers_Show

let ownerTest = "firebase"
let repoTest = "firebase-ios-sdk"

class Stargazers_ShowTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // Asynchronous test: Try to Retrieved Stargazers from Github API with a correct status code
    func testValidCallToGithubStargazersGetsHTTPStatusCode200() {
        // Url To test for good response
        let url = URL(string: "https://api.github.com/repos/\(ownerTest)/\(repoTest)/stargazers")
        //Expectation
        let expecation = expectation(description: "Status code: 200")
        // Try to retrieve data from Url
        URLSession.shared.dataTask(with: url!) { data, response, error in
            // If Error
            if let error = error {
                //Test Fail
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                //If Status Code Is the same of the Expectation
                if statusCode == 200 {
                    //Marks the expectation as having been met
                    expecation.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }.resume()
       //Set a max timeout before wait for expectation
        waitForExpectations(timeout: 5, handler: nil)
    }
}
